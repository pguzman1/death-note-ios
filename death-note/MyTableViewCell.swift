//
//  MyTableViewCell.swift
//  death-note
//
//  Created by Patricio GUZMAN on 10/3/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit


class MyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deathDetail: UILabel!
    @IBOutlet weak var nameOfPerson: UILabel!
    @IBOutlet weak var timeOfDeath: UILabel!
    @IBOutlet weak var myUiViewInCell: UIView!

    func setCell() {
        myUiViewInCell.layer.cornerRadius = 5;
        myUiViewInCell.layer.masksToBounds = true;
    }
}
