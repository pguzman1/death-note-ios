//
//  SecondViewController.swift
//  death-note
//
//  Created by Patricio GUZMAN on 10/4/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var details: UITextView!
    @IBOutlet var navigationBar: UIView!
    var dateToPass: String = ""
    var masterView : ViewController!
    var shouldIAddPerson: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.delegate = self
        
        navigationItem.title = "New Note"
        let date = Date()
        datePicker.minimumDate = date
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        dateToPass = formatter.string(from: date)
        
        details.layer.borderWidth = 1.0
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneEditing))
    }
    
    @IBAction func didPick(_ sender: UIDatePicker) {
        let date: Date = sender.date
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        self.dateToPass = formatter.string(from: date)
    }
    
    func doneEditing() {
        shouldIAddPerson = true
        print("name: \(self.name.text!)")
        print("date: \(self.dateToPass)")
        print("details: \(self.details.text!)")
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("\nview will disappear\n")
        let detailsToPass = details.text ?? ""
        if (name.text != "" && shouldIAddPerson) {

            masterView.newPeople = People(name: name.text, detailOfDeath: detailsToPass, timeOfDeath: dateToPass)
            print(masterView.newPeople!)
        }
    }
}

extension SecondViewController: UITextFieldDelegate {
// THIS IS ONE WAY TO LIMIT TEXTFIELD LENGTH
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maxLength = 10
//        let currentString: NSString = name.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        return newString.length <= maxLength
//    }
}
// THIS IS ANOTHER WAY TO LIMIT 
private var __maxLengths = [UITextField: Int]()

extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        let c = self.characters
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
}
