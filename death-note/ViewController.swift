//
//  ViewController.swift
//  death-note
//
//  Created by Patricio GUZMAN on 10/3/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
struct People {
    let name: String!
    let detailOfDeath: String!
    let timeOfDeath: String!
}
class ViewController: UIViewController {
    var peopleToDisplay = [People]()
    var newPeople: People?
    @IBOutlet weak var table: UITableView!
    
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        peopleToDisplay.append(People(name: "juan", detailOfDeath: "heart attack", timeOfDeath: "tomorrow"))
        peopleToDisplay.append(People(name: "pedro", detailOfDeath: "death by snu snu", timeOfDeath: "friday 26"))
        peopleToDisplay.append(People(name: "gabriel", detailOfDeath: "killed by voldemort", timeOfDeath: "this weekend"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("\nview will appear\n")
        if let toAdd = newPeople {
            print("\nappending\n")
            peopleToDisplay.append(toAdd)
            newPeople = nil
            table.estimatedRowHeight = 100
            table.rowHeight = UITableViewAutomaticDimension
            table.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "addButtonIdentifier") {
            print("\npreparing segue\n")
            let secondViewController: SecondViewController = segue.destination as! SecondViewController
            secondViewController.masterView = self
        }
    }
    
    @IBAction func addButtonClick(_ sender: UIBarButtonItem) {
        print("\npreforming segue\n")
        self.performSegue(withIdentifier: "addButtonIdentifier", sender: nil)
    }

}

extension ViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peopleToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell") as! MyTableViewCell
        cell.nameOfPerson.text = peopleToDisplay[indexPath.row].name
        cell.deathDetail.text = peopleToDisplay[indexPath.row].detailOfDeath
        cell.timeOfDeath.text = peopleToDisplay[indexPath.row].timeOfDeath
        cell.setCell()
        return cell
    }
    
}

